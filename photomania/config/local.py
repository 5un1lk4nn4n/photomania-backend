import os

from photomania.config.default import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ["*"]

CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_CREDENTIALS = True
# CORS_ORIGIN_WHITELIST = ('*',)
CORS_ORIGIN_REGEX_WHITELIST = ('*',)

# Mail
EMAIL_HOST = "localhost"
EMAIL_PORT = 1025
EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

# Database
# https://docs.djangoproject.com/en/4.2/ref/settings/#databases


DATABASES = {
    "default": {
        "ENGINE": 'django.db.backends.postgresql_psycopg2',
        "NAME": os.environ.get("DATABASE"),
        "USER": os.environ.get("DB_USER"),
        "PASSWORD": os.environ.get("DB_PASSWORD"),
        "HOST": os.environ.get("DB_HOST"),
        "PORT": os.environ.get("DB_PORT"),

    }
}

# Celery settings
CELERY_BROKER_URL = "redis://localhost:6379"
CELERY_RESULT_BACKEND = "redis://localhost:6379"

# Elastic search settings
ELASTICSEARCH_URL = os.environ.get("ELASTICSEARCH_URL")
# ELASTICSEARCH_INDEX = os.environ.get("ELASTICSEARCH_INDEX")
# ELASTICSEARCH_DOC_TYPE = os.environ.get("ELASTICSEARCH_DOC_TYPE")
ELASTICSEARCH_USERNAME = os.environ.get("ELASTICSEARCH_USERNAME")
ELASTICSEARCH_PASSWORD = os.environ.get("ELASTICSEARCH_PASSWORD")
# ELASTICSEARCH_CLIENT_CERT = os.environ.get("ELASTICSEARCH_CLIENT_CERT")

