# django_celery/celery.py
from __future__ import absolute_import, unicode_literals
import os
import django
from celery import Celery
from django.conf import settings


app = Celery("analyzer_celery")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "photomania.config.local")
app.config_from_object('django.conf:settings', namespace="CELERY")
app.autodiscover_tasks()
