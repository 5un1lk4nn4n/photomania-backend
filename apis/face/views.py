from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from apis.base.views import BaseApiView, BaseViewSet
from .serializers import FaceSerializer
from .models import Face
from rest_framework.permissions import IsAuthenticated, AllowAny
from apis.events.models import Event
# from .face import ImageFinder, FaceSearch, FaceAnalyzer, FaceMatch, CompressImage, ProcessImage
from django.conf import settings
from .utils import get_folder_name, get_event_image_folder, get_face_image_path, get_media_folder
import os
from apis.events.utils import get_event_or_sharing_status, EventStatus
import requests
from apis.filemanager.serializers import EventImageSerializer
from apis.filemanager.models import EventImage
from datetime import datetime
import json
import uuid
from .exceptions import InvalidQrCode, EventSharingExpired, EventSharingNotStarted, InvalidImage, \
    MoibleAlreadyRegistered


class FaceViewSet(BaseViewSet):
    queryset = Face.objects.all()
    serializer_class = FaceSerializer
    http_method_names = ["get", "post", 'delete', 'patch', 'put']


class FaceApiView(BaseApiView):
    permission_classes = [AllowAny]
    http_method_names = ['post']

    def post(self, request):
        has_error = False
        error_message = None
        data = request.data
        qr_code = data.get('qr_code', None)
        try:
            if qr_code is None:
                raise InvalidQrCode("QR code is required")
            event = Event.objects.get(qr_code=qr_code)
            sharing_status = get_event_or_sharing_status(event.start_date, event.photo_sharing_end_date)
            if sharing_status == EventStatus.completed:
                raise EventSharingExpired("Event photo sharing is expired for this event!")
            if sharing_status == EventStatus.upcoming:
                raise EventSharingNotStarted("Event photo sharing is not started yet!")
            face_image_id = data.get('face_image', None)
            if face_image_id is None:
                raise InvalidImage("Face image is not uploaded properly. Please try again")
            if Face.objects.filter(mobile=data['mobile'], event=event).exists():
                raise MoibleAlreadyRegistered("Face is already registered for this event and mobile!")
            face = EventImage.objects.get(file_id=face_image_id)

            ## Below lines of code is used to check where photo has face
            # face_url = request.get_host()
            # print(face_url)
            # str(request.build_absolute_uri(face.file))
            # headers = {'Accept': 'application/json'}
            # body = json.dumps({'selfie_image_url': face_url})
            # response = requests.post("http://localhost:8000/api/check-selfie/", data=body, headers=headers)
            # print(response.text)

            data['event'] = event.id
            data['url_code'] = uuid.uuid4().hex
            serializer = FaceSerializer(data=data)
            if serializer.is_valid():
                serializer.save()
                return Response({"message": "Registration completed.", "code": data['url_code']},
                                status=status.HTTP_200_OK)
            else:
                error_message = serializer.errors
                has_error = True
        except Event.DoesNotExist:
            has_error, error_message = True, "QR Code is invalid"
        except InvalidQrCode as error:
            has_error, error_message = True, str(error)
        except EventSharingExpired as error:
            has_error, error_message = True, str(error)
        except EventSharingNotStarted as error:
            has_error, error_message = True, str(error)
        except InvalidImage as error:
            has_error, error_message = True, str(error)
        except MoibleAlreadyRegistered as error:
            has_error, error_message = True, str(error)

        return Response({"error": error_message, "has_error": has_error}, status=status.HTTP_400_BAD_REQUEST)


class MarriageSnapshotApiView(BaseApiView):
    permission_classes = [AllowAny]
    http_method_names = ['get']

    def get(self, request, event_id, face_id):
        images = []
        matched_images = []
        data = request.query_params
        try:
            media_folder = get_media_folder()
            face = Face.objects.get(id=face_id)

        except Face.DoesNotExist:
            pass
        except Event.DoesNotExist:
            return Response(
                {
                    "message": "Not Registered",
                    "code": 400,
                },
                status=status.HTTP_400_BAD_REQUEST,
            )

        return Response(
            {
                "data": matched_images,
                "message": "Faces matched",
                "code": 200,
            },
            status=status.HTTP_200_OK,
        )
