class NoFaceDetected(Exception):
    pass

class InvalidImage(Exception):
    pass

class InvalidImagePath(Exception):
    pass

class InvalidQrCode(Exception):
    pass

class EventSharingExpired(Exception):
    pass

class EventSharingNotStarted(Exception):
    pass

class MoibleAlreadyRegistered(Exception):
    pass