# from elasticsearch import Elasticsearch
# import os
#
# import cv2
# import face_recognition
# from PIL import Image
# from .face import FaceAnalyzer
# from django.conf import settings
#
#
# def connect_to_elastic_search():
#     client = Elasticsearch(
#         settings.ELASTICSEARCH_URL,
#         basic_auth=(settings.ELASTICSEARCH_USERNAME, settings.ELASTICSEARCH_PASSWORD),
#         verify_certs=False,
#         timeout=180
#     )
#     print(client.info())
#     return client
#
#
# def get_face_encodings(elastic_client, images, event_image_folder):
#     total_faces = 0
#
#     for image in images:
#         print(image)
#         path = os.path.join(event_image_folder, image)
#         image = face_recognition.load_image_file(path)
#         face_locations = face_recognition.face_locations(image)
#         total_faces += len(face_locations)
#
#         # encode the 128-dimension face encoding for each face in the image
#         face_encodings = face_recognition.face_encodings(image, face_locations)
#         # Display the 128-dimension for each face detected
#         for face_encoding in face_encodings:
#             print("got encoding!")
#             doc = {
#                 "image": image,
#                 "face_encoding": face_encoding.tolist(),
#             }
#
#             # import json
#             # with open('face.json', 'w', encoding='utf-8') as f:
#             #     json.dump(face_encoding.tolist(), f, ensure_ascii=False, indent=4)
#             # break
#             elastic_client.index(index="faces", document=doc)
#             # break
#         # break
#     return total_faces
