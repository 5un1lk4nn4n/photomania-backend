import os

from django.conf import settings


def get_media_folder():
    return settings.MEDIA_ROOT


def get_event_image_folder(event_id):
    media_root = get_media_folder()
    event_root_folder = os.path.join(media_root, event_id)
    face_folder = get_folder_name(event_root_folder)
    face_folder = os.path.join(event_root_folder, face_folder)
    return face_folder


def get_folder_name(path):
    folder_name = ''
    for _, folders, _ in os.walk(path):
        folder_name = folders[0]
        break
    return folder_name


def get_face_image_path(image):
    return os.path.join(get_media_folder(), image)
