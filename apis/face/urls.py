from django.urls import path, include
from apis.face.views import FaceApiView, MarriageSnapshotApiView

urlpatterns = [
    path("", FaceApiView.as_view()),
    path("<int:event_id>/<int:face_id>/you", MarriageSnapshotApiView.as_view()),
]
