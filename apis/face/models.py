from django.db import models
from apis.base.models import ParanoidModel, TimeStampedModel
from apis.filemanager.models import EventImage
from apis.events.models import Event


class Face(TimeStampedModel):
    event = models.ForeignKey(
        Event,
        related_name="face_event",
        on_delete=models.RESTRICT,
        null=True
    )
    name = models.CharField(max_length=30, null=True, blank=True)
    mobile = models.CharField(max_length=12, null=False, blank=False)
    url_code = models.CharField(max_length=90, null=True, blank=False)
    face_image = models.ForeignKey(
        EventImage,
        related_name="face_image",
        on_delete=models.RESTRICT,
        null=True
    )

    class Meta:
        db_table = "faces"
        unique_together = ("event","mobile")
