# import os
#
# import cv2
# # import face_recognition
# from PIL import Image
# from .utils import get_media_folder
# from .exceptions import NoFaceDetected, InvalidImage, InvalidImagePath
#
#
# class FaceAnalyzer:
#     ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}
#
#     def __init__(self, image_path):
#         self.face_encoding = self.image = None
#         self.image = cv2.imread(self._is_valid_image(image_path))
#
#     @staticmethod
#     def _is_valid_image(path):
#         if not os.path.isfile(path) or os.path.splitext(path)[1][1:] not in FaceAnalyzer.ALLOWED_EXTENSIONS:
#             raise InvalidImage("Invalid image or image not found")
#         return path
#
#     @property
#     def is_face_detected(self):
#         face_locations = face_recognition.face_locations(self.image)
#         return len(face_locations) > 0
#
#     def get_face_encoding(self):
#         if self.is_face_detected:
#             f = face_recognition.face_encodings(self.image)
#             self.face_encoding = f[0]
#             return self.face_encoding
#         return None
#
#
# class ImageFinder:
#     extensions = ['.jpg', '.JPG']
#
#     def __init__(self, folder_path):
#         self.folder_path = folder_path
#         self.images = []
#
#     def get_images(self):
#
#         for _, _, files in os.walk(self.folder_path):
#             for image_file in files:
#                 self.images.append(image_file)
#         return self.images
#
#
# class Match:
#     def is_matched(self, img_encoding):
#         pass
#
#
# class FaceMatch(Match):
#     def __init__(self, face_encoding):
#         self.face_encoding = face_encoding
#
#     def log_accuracy(self, img_encoding):
#         distance = face_recognition.face_distance([self.face_encoding], img_encoding)
#         distance = round(distance[0] * 100)
#         # calculating accuracy level between images
#         accuracy = 100 - round(distance)
#         print(f"Accuracy Level: {accuracy}%")
#
#     def is_matched(self, img_encoding):
#         print(img_encoding, self.face_encoding)
#         if img_encoding is None or self.face_encoding is None:
#             return False
#         is_same = face_recognition.compare_faces([self.face_encoding], img_encoding)[0]
#         self.log_accuracy(img_encoding)
#         print("is_same", is_same)
#         return is_same
#
#
# class FaceSearch:
#     def __init__(self, image_list, event_image_folder):
#         self.image_list = image_list
#         self.event_image_folder = event_image_folder
#
#     def search(self, search_image: FaceMatch):
#         for image in self.image_list:
#             print("image", image)
#             path = os.path.join(self.event_image_folder, image)
#             print(path)
#             f = FaceAnalyzer(path)
#             if search_image.is_matched(f.get_face_encoding()):
#                 yield image
#
#
# class CompressImage:
#     COMPRESS_QUALITY = 90
#     RESIZE_PERCENT = 20  # reduce percentage
#
#     def __init__(self, image_path, output_dir=None):
#         self.image_path = image_path
#         self.image = self.get_image()
#         self.output_dir = get_media_folder() if output_dir is None else output_dir
#
#     def get_output_path(self):
#         return os.path.join(self.output_dir, self.get_file_name())
#
#     def get_file_name(self):
#         return os.path.basename(self.image_path)
#
#     def resize_image(self):
#         reduce_width = (CompressImage.RESIZE_PERCENT / 100) * float(self.image.size[0])
#         reduce_height = (CompressImage.RESIZE_PERCENT / 100) * float(self.image.size[1])
#
#         width = float(self.image.size[0]) - reduce_width
#         height = float(self.image.size[1]) - reduce_height
#
#         print(f"original size - {self.image.size[0]}*{self.image.size[1]}----->reduced size - {width}*{height}")
#
#         return self.image.resize((int(width), int(height)), Image.Resampling.LANCZOS)
#
#     def get_image(self):
#         image = Image.open(self.image_path)
#
#         # # downsize the image with an ANTIALIAS filter (gives the highest quality)
#         # image = image.resize((160, 300), Image.ANTIALIAS)
#         return image
#
#     def optimize_image(self):
#         print("optimising..")
#         print(self.get_output_path())
#         image = self.resize_image()
#         self.image = image
#         # foo.save('path/to/save/image_scaled.jpg', quality=95)  # The saved downsized image size is 24.8kb
#         self.image.save(self.get_output_path(), optimize=True,
#                         quality=CompressImage.COMPRESS_QUALITY)  # The saved downsized image size is 22.9kb
#
#
# class ProcessImage:
#     @staticmethod
#     def optimize_images(images, image_folder):
#         for image in images:
#             print("image", image)
#             path = os.path.join(image_folder, image)
#             out = os.path.join(image_folder, "compressed")
#             if not os.path.exists(out):
#                 os.makedirs(out)
#             print(path)
#             optimised = CompressImage(path, out)
#             optimised.optimize_image()
