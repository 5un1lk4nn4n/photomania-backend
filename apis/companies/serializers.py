from rest_framework import serializers
from .models import CompanySignup


class CompanySignupSerializer(serializers.ModelSerializer):
    class Meta:
        model = CompanySignup
        fields = "__all__"
