from django.contrib import admin

from .models import CompanySignup


@admin.register(CompanySignup)
class CompanySignupAdmin(admin.ModelAdmin):
    list_display = ("company", "mobile", 'created','status')
    list_filter = ["status"]
