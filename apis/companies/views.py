from apis.base.views import BaseApiView, BaseViewSet
from .models import CompanySignup
from .serializers import CompanySignupSerializer
from rest_framework.permissions import AllowAny


class CompanyViewSet(BaseViewSet):
    permission_classes = [AllowAny]
    queryset = CompanySignup.objects.all()
    serializer_class = CompanySignupSerializer
