from django.db import models
from apis.base.models import ParanoidModel, TimeStampedModel
from django.utils.translation import gettext_lazy as _


class CompanySignup(TimeStampedModel, ParanoidModel):
    STATUS_CHOICES = (
        (1, "enquiry"),
        (2, "registered"),
        (3, "cancelled"),
    )

    company = models.CharField(max_length=30, null=False, blank=False)
    mobile = models.CharField(_("mobile"), unique=True, null=False, blank=False)
    status = models.IntegerField(
                                 choices=STATUS_CHOICES,
                                 default=1)

    class Meta:
        db_table = "company_signups"
