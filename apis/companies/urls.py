from django.urls import path, include
from .views import CompanyViewSet

from rest_framework import routers
router = routers.DefaultRouter(trailing_slash=False)
router.register("", CompanyViewSet)
urlpatterns = router.urls