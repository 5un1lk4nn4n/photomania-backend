from django.db import models
from django.utils import timezone


class TimeStampedModel(models.Model):
    """
    TimeStampedModel
    An abstract base class model that provides self-managed "created" and
    "modified" fields.
    """

    created = models.DateTimeField(auto_now_add=True, null=True)
    modified = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        get_latest_by = "modified"
        ordering = (
            "-modified",
            "-created",
        )
        abstract = True


class ParanoidModel(models.Model):
    """
    Paranoid Model implements soft delete operations to models
    """

    deleted = models.DateTimeField(null=True, blank=True)

    class Meta:
        abstract = True

    def delete(self):
        self.deleted = timezone.now()
        self.save()

    def restore(self):
        self.deleted = None
        self.save()
