from rest_framework import pagination, permissions, viewsets
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.settings import api_settings
from rest_framework.views import APIView


class BaseViewSet(viewsets.ModelViewSet):
    # permission_classes = [
        # permissions.IsAuthenticated,
        # permissions.DjangoModelPermissions,
    # ]

    def get_queryset(self):
        qs = self.queryset.filter()
        return qs

    def perform_create(self, serializer):
        serializer.save()


class BaseApiView(APIView,PageNumberPagination):
    # permission_classes = [permissions.IsAuthenticated]
    pass



