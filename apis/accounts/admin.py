from django.contrib import admin
from .models import User, Company
from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelForm
# from django.contrib.auth.admin import UserAdmin
from django import forms
from apis.events.models import Event
from django.contrib.auth.hashers import make_password


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "get_events")
    search_fields = ['name', ]

    def get_events(self,obj):
        return Event.objects.filter(company=obj).count()


class UserAdminForm(ModelForm):
    class Meta:
        model = User
        fields = [
            "mobile",
            "company",
            "is_company_contact",
            "is_active",
            "password"
        ]

    def save(self, commit=True):
        self.instance.password = make_password(self.cleaned_data['password'])
        return super().save(commit)


class CompanyChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return f"{obj.name}"


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ("mobile", "get_company", 'is_active', 'is_company_contact', 'date_joined')
    list_filter = ["is_active", 'is_company_contact']
    search_fields = ['mobile', ]
    form = UserAdminForm

    @staticmethod
    def get_company(obj):
        return obj.company.name if obj.company else "-"

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'company':
            return CompanyChoiceField(queryset=Company.objects.all())
        return super().formfield_for_foreignkey(db_field, request, **kwargs)
