from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from apis.base.models import ParanoidModel, TimeStampedModel
from .managers import CustomUserManager


class Company(TimeStampedModel, ParanoidModel):
    name = models.CharField(max_length=30, null=False, blank=False)
    address = models.CharField(max_length=255, null=False, blank=False)
    pincode = models.CharField(max_length=6, null=False, blank=False)

    class Meta:
        db_table = "companies"


class User(AbstractBaseUser, PermissionsMixin):
    mobile = models.CharField(_("mobile"), unique=True, null=False, blank=False)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(default=timezone.now)
    company = models.ForeignKey(
        Company,
        related_name="user_company",
        on_delete=models.RESTRICT,
        null=True
    )
    is_company_contact = models.BooleanField(default=False)
    USERNAME_FIELD = "mobile"
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return self.mobile
