from datetime import datetime
from .models import Event
from django.db.models import Q, Count
from enum import Enum


class EventStatus(Enum):
    active = "Active"
    completed = "Completed"
    upcoming = "Upcoming"
    unknown = "Unknown"


def get_event_or_sharing_status(start_date, end_date):
    today = datetime.today().date()
    if start_date <= today <= end_date:
        return EventStatus.active
    if end_date < today:
        return EventStatus.completed
    if start_date > today:
        return EventStatus.upcoming
    return EventStatus.unknown


def get_active_events(event_instances: list[Event]) -> list[Event]:
    print("Getting", event_instances)
    today = datetime.today().date()
    return event_instances.filter(
        start_date__lte=today, end_date__gte=today
        # Q(start_date__gte=today) | Q(end_date__lte=today)
    )


def get_completed_events(event_instances: list[Event]) -> list[Event]:
    today = datetime.today().date()
    return event_instances.filter(
        Q(end_date__lt=today)
    )


def get_upcoming_events(event_instances: list[Event]) -> list[Event]:
    today = datetime.today().date()
    return event_instances.filter(
        Q(start_date__gt=today)
    )


def get_active_sharing_events(event_instances: list[Event]) -> list[Event]:
    today = datetime.today().date()
    return event_instances.filter(
        start_date__lte=today, photo_sharing_end_date__lte=today
    )


def get_completed_sharing_events(event_instances: list[Event]) -> list[Event]:
    today = datetime.today().date()
    return event_instances.filter(
        Q(photo_sharing_end_date__lt=today)
    )


def get_upcoming_sharing_events(event_instances: list[Event]) -> list[Event]:
    today = datetime.today().date()
    return event_instances.filter(
        Q(start_date__gt=today)
    )
