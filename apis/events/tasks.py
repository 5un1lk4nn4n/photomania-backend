from time import sleep

from photomania.celery import app
from celery import Task


class GalleryUrl(Task):
    def run(self):
        sleep(20)  # Simulate expensive operation(s) that freeze Django
        print('generation URL')


app.register_task(GalleryUrl())
