# Generated by Django 4.2.6 on 2023-10-12 13:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0005_alter_event_company'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='qr_code',
            field=models.CharField(max_length=90, unique=True),
        ),
    ]
