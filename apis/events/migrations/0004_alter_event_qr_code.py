# Generated by Django 4.2.6 on 2023-10-12 10:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0003_alter_event_qr_code'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='qr_code',
            field=models.CharField(editable=False, max_length=90, unique=True),
        ),
    ]
