import os
import uuid
from rest_framework import filters
from django.conf import settings
from django.db.models import Count
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.response import Response
from django.db.models import Q
from apis.accounts.models import Company
from apis.base.views import BaseApiView, BaseViewSet
from apis.face.models import Face
from rest_framework.permissions import IsAuthenticated, AllowAny
from .downloader import GoogleDriveDownloaderBySession
from .models import Event
from .permissions import IsEventOwner
from .serializers import (CompanySerializer, EventDetailSerializer,
                          EventSerializer, EventMenuSerializer)
from .utils import (get_active_events, get_completed_events, get_upcoming_events, EventStatus,
                    get_completed_sharing_events, get_active_sharing_events, get_upcoming_sharing_events)

from apis.events.utils import get_event_or_sharing_status, EventStatus
from apis.face.exceptions import InvalidQrCode, EventSharingExpired, EventSharingNotStarted, InvalidImage, \
    MoibleAlreadyRegistered


class CompanyViewSet(BaseViewSet):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer


class EventViewSet(BaseViewSet):
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    http_method_names = ["get", "post", 'delete', 'patch', 'put']

    def get_serializer_class(self):
        if self.action == 'list':
            return EventDetailSerializer
        if self.action == 'retrieve':
            return EventDetailSerializer
        return EventSerializer

    def create(self, request, *args, **kwargs):
        data = request.data


class EventApiView(BaseApiView):
    http_method_names = ['get', 'post']

    def get(self, request):
        user = request.user
        company = user.company
        data = request.query_params
        events = Event.objects.filter(company=company)
        if event_status := data.get('status', None):
            if event_status == EventStatus.active.value:
                events = get_active_events(events)
            if event_status == EventStatus.upcoming.value:
                events = get_upcoming_events(events)
            if event_status == EventStatus.completed.value:
                events = get_completed_events(events)
        if sharing_status := data.get('sharing', None):

            if sharing_status == EventStatus.active.value:
                events = get_active_sharing_events(events)
            if sharing_status == EventStatus.upcoming.value:
                events = get_upcoming_sharing_events(events)
            if sharing_status == EventStatus.completed.value:
                events = get_completed_sharing_events(events)
        if search := data.get('search', None):
            events = events.filter(Q(qr_code__icontains=search) | Q(name__icontains=search))

        results = self.paginate_queryset(events, request, view=self)
        serializer = EventDetailSerializer(results, many=True, context={"request": request})
        return self.get_paginated_response(serializer.data)

    def post(self, request):
        user = request.user
        company = user.company
        data = request.data
        data['company'] = company.id
        data['qr_code'] = uuid.uuid4().hex
        serializer = EventSerializer(data=data)
        if serializer.is_valid():
            serializer.save()

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class EventDetailApiView(BaseApiView):
    http_method_names = ['get', 'put']
    permission_classes = [IsEventOwner]

    def get(self, request, event_id):
        event = get_object_or_404(Event, pk=event_id)
        self.check_object_permissions(request, event)
        serializer = EventDetailSerializer(event, context={"request": request})
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, event_id):
        event = get_object_or_404(Event, pk=event_id)
        self.check_object_permissions(request, event)
        data = request.data

        # don't allow user to modify these fields
        _ = data.pop("company", None)
        _ = data.pop("qr_code", None)

        serializer = EventSerializer(event, data=data, partial=True)
        if serializer.is_valid():
            response_serializer = EventDetailSerializer(serializer.save(), context={"request": request})
            return Response(response_serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class EventQrValidatorApiView(BaseApiView):
    http_method_names = ['get', ]
    permission_classes = [AllowAny]

    def get(self, request, qr_code):
        event = get_object_or_404(Event, qr_code=qr_code)

        sharing_status = get_event_or_sharing_status(event.start_date, event.photo_sharing_end_date)
        if sharing_status == EventStatus.completed:
            return Response({
                "code": 1,
                "message": "Event Expired"
            }, status=status.HTTP_400_BAD_REQUEST)

        if sharing_status == EventStatus.upcoming:
            return Response({
                "code": 2,
                "message": "Event not started"
            }, status=status.HTTP_400_BAD_REQUEST)

        return Response({
            "name": event.name,
            "poster": request.build_absolute_uri(event.poster.file.url) if event.poster and event.poster.file else None

        }, status=status.HTTP_200_OK)


class EventAlbumValidatorApiView(BaseApiView):
    http_method_names = ['get', ]
    permission_classes = [AllowAny]

    def get(self, request, code):
        face = get_object_or_404(Face, url_code=code)
        event = face.event
        sharing_status = get_event_or_sharing_status(event.start_date, event.photo_sharing_end_date)
        if sharing_status == EventStatus.completed:
            return Response({
                "code": 1,
                "message": "Event Expired"
            }, status=status.HTTP_400_BAD_REQUEST)

        if sharing_status == EventStatus.upcoming:
            return Response({
                "code": 2,
                "message": "Event not started"
            }, status=status.HTTP_400_BAD_REQUEST)

        return Response({
            "selfie_url": request.build_absolute_uri(
                face.face_image.file.url),
            "code": event.qr_code,
            "name": event.name,
            "expires": event.photo_sharing_end_date,
            "poster": request.build_absolute_uri(event.poster.file.url) if event.poster and event.poster.file else None
        }, status=status.HTTP_200_OK)


class EventDashboardApiView(BaseApiView):
    http_method_names = ['get']

    def get(self, request):
        user = request.user
        company = user.company
        events = Event.objects.filter(company=company)
        total_events = events.count()
        active_events = get_active_events(events).count()
        upcoming_events = get_upcoming_events(events).count()
        completed_events = get_completed_events(events).count()

        face_data = Face.objects.filter(event__company=company).values('event__name').annotate(
            no_of_faces=Count('event')).order_by('no_of_faces')

        response = {
            "total_events": total_events,
            "active_events": active_events,
            "upcoming_events": upcoming_events,
            "completed_events": completed_events,
            "face_data": face_data
        }

        return Response(response, status=status.HTTP_200_OK)


class EventMenuApiView(BaseApiView):
    def get(self, request):
        user = request.user
        company = user.company
        events = Event.objects.filter(company=company)
        print(events)

        active_events = get_active_events(events)
        upcoming_events = get_upcoming_events(events)

        print(active_events, upcoming_events)
        active_event_serializer = EventMenuSerializer(active_events, many=True)
        upcoming_event_serializer = EventMenuSerializer(upcoming_events, many=True)
        return Response({
            "active_events": active_event_serializer.data,
            "upcoming_events": upcoming_event_serializer.data

        }, status=status.HTTP_200_OK)


class DownloaderApiView(BaseApiView):

    def get(self, request, event_id):
        event = get_object_or_404(Event, pk=event_id)
        event_photos = GoogleDriveDownloaderBySession(event.data_source)
        print("validated")
        print(event_photos.is_valid_url())
        media_root = settings.MEDIA_ROOT
        # output_folder = f"{media_root}/{event.qr_code}/"
        output_folder = os.path.join(media_root, event.qr_code)

        if event_photos.is_valid_url():
            event_photos.download(output_folder)
            # GalleryUrl.delay()
        return Response(
            {
                "message": "Registered",
                "code": 200,
            },
            status=status.HTTP_400_BAD_REQUEST,
        )
