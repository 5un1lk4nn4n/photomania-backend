from urllib.parse import urlparse

import gdown
import requests
from django.core.exceptions import ValidationError
from django.core.validators import URLValidator


class Downloader:
    def __init__(self, url):
        self.url = url

    def is_valid_url(self):
        validate = URLValidator()
        print(self.url)
        try:
            validate(self.url)
        except ValidationError as e:
            print(e)
            return False
        return True

    def download(self, output_folder):
        pass


class GoogleDriveDownloader(Downloader):
    domains = ['drive.google.com']

    def is_valid_url(self):
        print(urlparse(
            self.url).hostname)
        return super(GoogleDriveDownloader, self).is_valid_url() and urlparse(
            self.url).hostname in GoogleDriveDownloader.domains

    def download(self, output_folder):
        print(self.is_valid_url())
        if self.is_valid_url():
            gdown.download_folder(self.url, quiet=True, use_cookies=False, remaining_ok=True)


class GoogleDriveDownloaderBySession(GoogleDriveDownloader):
    CHUNK_SIZE = 32768

    @staticmethod
    def save_response_content(response, destination):

        with open(destination, "wb") as f:
            for chunk in response.iter_content(GoogleDriveDownloaderBySession.CHUNK_SIZE):
                if chunk:  # filter out keep-alive new chunks
                    f.write(chunk)

    @staticmethod
    def get_confirm_token(response):
        for key, value in response.cookies.items():
            if key.startswith("download_warning"):
                return value

        return None

    def download(self, output_folder):
        print(self.is_valid_url())
        if self.is_valid_url():
            drive_id = self.url.rsplit('/', 1)[-1]
            URL = "https://docs.google.com/uc?export=download&confirm=1"

            session = requests.Session()

            response = session.get(URL, params={"id": drive_id}, stream=True)
            token = self.get_confirm_token(response)

            if token:
                params = {"id": drive_id, "confirm": token}
                response = session.get(URL, params=params, stream=True)

            self.save_response_content(response, output_folder)

