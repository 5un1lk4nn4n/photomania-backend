from django.urls import path, include
from .views import (
    EventViewSet,
    DownloaderApiView,
    EventApiView,
    CompanyViewSet,
    EventDetailApiView,
    EventDashboardApiView,
    EventMenuApiView,
    EventQrValidatorApiView,
    EventAlbumValidatorApiView
)
from rest_framework import routers

router = routers.DefaultRouter(trailing_slash=False)

router.register("company", CompanyViewSet)

urlpatterns = ([
                   path("trigger-download/<int:event_id>", DownloaderApiView.as_view()),
                   path("<int:event_id>", EventDetailApiView.as_view()),
                   path("qr-code/<str:qr_code>", EventQrValidatorApiView.as_view()),
                   path("album/<str:code>", EventAlbumValidatorApiView.as_view()),
                   path("dashboard", EventDashboardApiView.as_view()),
                   path("select-menu", EventMenuApiView.as_view()),
                   path("", EventApiView.as_view()),
               ] + router.urls)
