from rest_framework import serializers
from .models import Event
from datetime import datetime
from .utils import get_event_or_sharing_status
from apis.accounts.models import Company


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = "__all__"


class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = "__all__"


class EventDetailSerializer(serializers.ModelSerializer):
    poster = serializers.SerializerMethodField()
    poster_url = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()
    sharing_status = serializers.SerializerMethodField()

    class Meta:
        model = Event
        fields = (
            "id", "name", "poster", "poster_url", "qr_code", "data_source", "start_date", "end_date",
            "photo_sharing_end_date", 'sharing_status', 'status')

    def get_poster(self, obj):
        return obj.poster.file_id if obj.poster and obj.poster.file else None

    def get_poster_url(self, obj):
        request = self.context.get('request')
        return request.build_absolute_uri(obj.poster.file.url) if obj.poster and obj.poster.file else None

    def get_status(self, obj):
        return get_event_or_sharing_status(obj.start_date, obj.end_date).value

    def get_sharing_status(self, obj):
        return get_event_or_sharing_status(obj.start_date, obj.photo_sharing_end_date).value


class EventMenuSerializer(serializers.ModelSerializer):
    """
    Dropdown menu with keys value and label (select element)
    """
    value = serializers.IntegerField(source='id')
    label = serializers.CharField(source='name')

    class Meta:
        model = Event
        fields = ('value', 'label')
