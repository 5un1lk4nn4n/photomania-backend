from django.db import models
from apis.base.models import ParanoidModel, TimeStampedModel
from apis.filemanager.models import EventImage
from datetime import datetime, timedelta
from apis.accounts.models import Company


class Event(TimeStampedModel, ParanoidModel):
    qr_code = models.CharField(max_length=90, null=False, blank=False, unique=True)
    name = models.CharField(max_length=30, null=False, blank=False)
    poster = models.ForeignKey(
        EventImage,
        related_name="event_poster",
        on_delete=models.RESTRICT,
        null=True
    )
    data_source = models.CharField(max_length=255, null=True, blank=False)
    start_date = models.DateField(null=True)
    end_date = models.DateField(null=True)
    photo_sharing_end_date = models.DateField(null=True)
    company = models.ForeignKey(
        Company,
        related_name="event_company",
        on_delete=models.RESTRICT,
        null=True,
    )

    class Meta:
        db_table = "events"
        ordering = ("-modified", "-created")
