from django.db import models
from django.utils import timezone
from apis.base.models import TimeStampedModel


class EventImage(TimeStampedModel):
    file_id = models.AutoField(primary_key=True)
    file = models.FileField(null=True, max_length=255)
    date_created = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return str(self.file.name)
    class Meta:
        db_table = "event_images"
