from apis.base.views import BaseApiView, BaseViewSet
from .models import EventImage
from .serializers import EventImageSerializer


class EventImageViewSet(BaseViewSet):
    queryset = EventImage.objects.all()
    serializer_class = EventImageSerializer
