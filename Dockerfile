FROM python:3.10-slim-bookworm

WORKDIR /app

RUN apt-get update && apt-get install ffmpeg libsm6 libxext6 netcat-traditional  -y

COPY pyproject.toml /app

RUN pip3 install poetry

RUN poetry config virtualenvs.create false

RUN poetry install

COPY . /app

# Collect static files
RUN python -m manage collectstatic -v 3 --no-input
